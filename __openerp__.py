{
    'name': 'Jamotion Base Settings',
    'version': '8.0.1.3.0',
    'category': 'Uncategorized',
    'description': "Jamotion style settings for Jamotion hosted installations",
    'author': 'Jamotion GmbH',
    'website': 'http://www.jamotion.ch',
    'depends': [
        "web",
        "web_dialog_size",
    ],
    'data': [
        "views/base_jamotion.xml",
        "views/res_country_view.xml",
        "views/res_partner_view.xml",
        "base_jamotion_data.xml",
    ],
    'auto_install': True,
    'installable': True,
    'css': [
        "static/src/css/base.css"
    ],
    'qweb': [
        "static/src/xml/base.xml"
    ],
}
