Changelog
=========

Version 8.0.1.2.0
-----------------
    - Add field "hide_parent_name" (moved from module partner_division)
    - Remove displaying of generated addresses from partner form view
    - Refactor address generation: no store, no depend

Version 8.0.1.1.0
-----------------
Feature 1741:
    - Generate print addresses for invoice and delivery documents on country

Version 8.0.1.0.1
-----------------

Version 8.0.1.0.0
-----------------
Initial Release
