# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 02.08.2015.
#
import json
import logging
import os
import re
import traceback
from datetime import date, datetime

from hypchat import HypChat

import openerp
from openerp import models, fields, api
from openerp.service import db

_logger = logging.getLogger(__name__)


class BaseJamotionDumper(models.TransientModel):
    _name = 'base_jamotion.dumper'
    _description = 'Helper Model of Jamotion'

    @api.model
    def clean_dumps(self, daystokeep=2):
        backup_path = os.path.join(openerp.tools.config['data_dir'], 'backup')
        if not os.path.exists(backup_path):
            return

        for f in os.listdir(backup_path):
            fullpath = os.path.join(backup_path, f)
            timestamp = os.stat(fullpath).st_mtime
            createtime = datetime.fromtimestamp(timestamp)
            now = datetime.now()
            delta = now - createtime
            if delta.days >= daystokeep and os.path.isfile(fullpath):
                os.remove(fullpath)

    @api.model
    def _send_notification(self, msg=u'', type='info', notify=False):
        notification_type = self.env['ir.config_parameter'].get_param(
                'base_jamotion.notification_type', False)
        if notification_type == 'hipchat':
            self._send_hipchat_notification(msg, type, notify)
        if notification_type == 'odoochat':
            self._send_odoochat_notification(msg, type, notify)

    @api.model
    def _send_odoochat_notification(self, msg, type, notify=False):
        msg_obj = self.env['im_chat.message']
        userid = self.env['ir.config_parameter'].get_param(
                'base_jamotion.odoochat_uid', False)
        msg_obj.post(self._uid, userid, 'message', msg)

    @api.model
    def _send_hipchat_notification(self, msg, type, notify=False):
        token = self.env['ir.config_parameter'].get_param(
                'base_jamotion.hipchat_token', False)
        room = self.env['ir.config_parameter'].get_param(
                'base_jamotion.hipchat_room', False)
        if type == 'info':
            col = 'green'
        elif type == 'warning':
            col = 'yellow'
        else:
            col = 'red'
        hc = HypChat(token)
        room = hc.get_room(room)
        room.notification(
                message=msg,
                color=col,
                format='html',
                notify=notify,
        )

    @api.model
    def create_dump(self, db_regex='^prod$'):
        db_check = re.compile(db_regex)
        if not db_check.match(self.env.cr.dbname):
          _logger.info('Automatic dump for db {db} disabled'.format(self.env.cr.dbname))
          return
          
        dbname = self.env.cr.dbname
        company = self.env['res.users'].browse(self.env.uid).company_id.name
        try:
            backup_path = os.path.join(openerp.tools.config['data_dir'],
                                       'backup')
            if not os.path.exists(backup_path):
                os.mkdir(backup_path)

            backup_file = os.path.join(backup_path, 'backup_%s_%s.dump' % (
                dbname, date.today()))
            with open(backup_file, 'w') as t:
                db.dump_db(dbname, t, 'psql')

            manifest_file = os.path.join(backup_path, 'manifest.json')
            with open(manifest_file, 'w') as fh:
                json.dump(db.dump_db_manifest(self.env.cr), fh, indent=4)
            msg = u'<p>Backup at {company} for database {db} sucessfully ' \
                  u'created.</p>'.format(
                    company=company,
                    db=dbname,
            )
            self._send_notification(msg)
            self.clean_dumps()
        except:
            exc = traceback.format_exc()
            msg = u'<p>Backup at {company} of database {db} failed!</p>' \
                  u'<p><b>Traceback:</b></p>' \
                  u'<pre>{trace}</pre>'.format(
                    company=company,
                    db=dbname,
                    trace=exc,
            )
            self._send_notification(msg, 'error', True)

    @api.model
    def restore_dump(self):
        restore_path = os.path.join(openerp.tools.config['data_dir'], 'backup')
        restore_file = os.path.join(restore_path,
                                    'backup_%s.dump' % self.env.cr.dbname)
        new_db_name = '%s_%s' % (self.env.cr.dbname, fields.Date.today())
        db.restore_db(new_db_name, restore_file)
