# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 17.03.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp.tools import safe_eval

from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class ResCountry(models.Model):
    # Private attributes
    _inherit = 'res.country'

    # Default methods

    # Fields declaration

    address_block_base = fields.Text(
            string="Base Block",
    )

    address_line_base = fields.Text(
            string="Base Line",
    )

    address_block_invoice = fields.Text(
            string="Invoice Block",
    )

    address_line_invoice = fields.Text(
            string="Invoice Line",
    )

    address_block_delivery = fields.Text(
            string="Delivery Block",
    )

    address_line_delivery = fields.Text(
            string="Delivery Line",
    )

    address_invoice_use = fields.Boolean(
            string="Use Invoice Addresses",
    )

    address_delivery_use = fields.Boolean(
            string="Use Delivery Addresses",
    )

    # compute and search fields, in the same order that fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
    def generate_address_block(self, partner, type='base'):
        block_pattern = getattr(self, 'address_block_{0}'.format('base'))

        if ((type == 'invoice' and self.address_invoice_use) or
                (type == 'delivery' and self.address_delivery_use)):
            block_pattern = getattr(self, 'address_block_{0}'.format(type))

        if not block_pattern or block_pattern == '':
            return False

        line_patterns = block_pattern.split('\n')
        lines = []
        for line_pattern in line_patterns:
            line = self._generate_address_line(partner, line_pattern)
            if line:
                lines.append(line)
        result = '\n'.join(lines)
        return result

    def generate_address_line(self, partner, type='base'):
        line_pattern = getattr(self, 'address_line_{0}'.format('base'))

        if ((type == 'invoice' and self.address_invoice_use) or
                (type == 'delivery' and self.address_delivery_use)):
            line_pattern = getattr(self, 'address_line_{0}'.format(type))

        return self._generate_address_line(partner, line_pattern)

    def _generate_address_line(self, partner, line_pattern):
        self.ensure_one()
        if not line_pattern:
            return False

        result = False
        line = safe_eval(line_pattern, {'adr': partner})
        if isinstance(line, tuple):
            separator = line[0]
            field_list = line[1]
            result_list = [f for f in field_list if isinstance(f, basestring)]
            result = separator.join(result_list)
        elif isinstance(line, list):
            field_list = line
            result_list = [f for f in field_list if isinstance(f, basestring)]
            result = ' '.join(result_list)
        elif isinstance(line, basestring):
            result = line

        return result
