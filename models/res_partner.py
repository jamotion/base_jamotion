# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 17.03.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    # Private attributes
    _inherit = 'res.partner'

    # Default methods

    # Fields declaration
    hide_parent_name = fields.Boolean(
            string='Hide Company Name',
    )

    address_block_base = fields.Text(
            string="Base Block",
            compute='_compute_address_for_print',
    )

    address_line_base = fields.Char(
            string="Base Line",
            compute='_compute_address_for_print',
    )

    address_block_invoice = fields.Text(
            string="Invoice Block",
            compute='_compute_address_for_print',
    )

    address_line_invoice = fields.Char(
            string="Invoice Line",
            compute='_compute_address_for_print',
    )

    address_block_delivery = fields.Text(
            string="Delivery Block",
            compute='_compute_address_for_print',
    )

    address_line_delivery = fields.Char(
            string="Delivery Line",
            compute='_compute_address_for_print',
    )

    # compute and search fields, in the same order as fields declaration
    @api.multi
    def _compute_address_for_print(self):
        for record in self:
            country = record.country_id
            if not country:
                continue

            record.address_line_base = country.generate_address_line(record)
            record.address_block_base = country.generate_address_block(record)

            record.address_line_invoice = country.generate_address_line(
                    record,
                    'invoice',
            )
            record.address_block_invoice = country.generate_address_block(
                    record,
                    'invoice',
            )
            record.address_line_delivery = country.generate_address_line(
                    record,
                    'delivery',
            )
            record.address_block_delivery = country.generate_address_block(
                    record,
                    'delivery',
            )

            # Constraints and onchanges

            # CRUD methods

            # Action methods

            # Business methods

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        if not context.get('check_hide_parent_name', False):
            return super(ResPartner, self).name_get(cr, uid, ids, context)

        res = []
        for record in self.browse(cr,uid, ids, context=context):
            name = record.name
            if record.parent_id and \
                    not record.is_company and \
                    not record.hide_parent_name:
                name = "%s, %s" % (record.parent_name, name)
            if context.get('show_address_only'):
                name = self._display_address(cr, uid, record, without_company=True, context=context)
            if context.get('show_address'):
                name = name + "\n" + self._display_address(cr, uid, record, without_company=True, context=context)
            name = name.replace('\n\n', '\n')
            name = name.replace('\n\n', '\n')
            if context.get('show_email') and record.email:
                name = "%s <%s>" % (name, record.email)
            res.append((record.id, name))
        return res
