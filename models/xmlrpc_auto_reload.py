# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo on 10.11.2017.
#
# imports of python lib
import logging

# imports of openerp
import openerp
from openerp import models, api
# global variable definitions
from openerp.service.server import restart, _reexec

# imports from odoo modules

_logger = logging.getLogger(__name__)


class XmlrpcAutoReload(models.TransientModel):
    # Private attributes
    _name = 'xmlrpc.auto.reload'
    _description = 'xmlrpc autorelad'

    @api.model
    def reload_files(self, files):
        py_files = self._process_python(files)
        modules = self._process_data(files)
        if modules:
            _logger.info('XML Changes detected - reloading modules')
            mods = self.env['ir.module.module'].search([('name', 'in', modules.keys())])
            mods.write({'state': 'to upgrade'})
        if py_files:
            _logger.info('Python Changes detected - restarting odoo')
        if modules or py_files:
            restart()

        return True

    @api.model
    def _process_data(self, files):
        modules = {}
        xml_files = [i for i in files if i.endswith('.xml')]
        for i in xml_files:
            for path in openerp.modules.module.ad_paths:
                if i.startswith(path):
                    # find out wich addons path the file belongs to
                    # and extract it's module name
                    right = i[len(path) + 1:].split('/')
                    if len(right) < 2:
                        continue
                    module = right[0]
                    modules[module] = 1
        return modules

    @api.model
    def _process_python(self, files):
        # process python changes
        py_files = [i for i in files if i.endswith('.py')]
        py_errors = []
        # TODO keep python errors until they are ok
        if py_files:
            for i in py_files:
                try:
                    source = open(i, 'rb').read() + '\n'
                    compile(source, i, 'exec')
                except SyntaxError:
                    py_errors.append(i)
            if py_errors:
                _logger.info(
                    'autoreload: python code change detected, errors found')
                for i in py_errors:
                    _logger.info('autoreload: SyntaxError %s', i)
                return False
            else:
                _logger.info(
                    'autoreload: python code updated, autoreload activated')
                return True
        return False
