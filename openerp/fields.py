# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2018 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 27.12.2016.
#
# imports of python lib
import datetime

import pytz

from openerp import fields
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT
from openerp.tools import DEFAULT_SERVER_TIME_FORMAT as TIME_FORMAT


@staticmethod
def date_from_string_to_lang(value, env):
    date = fields.Date.from_string(value)
    l = env.context.get('lang', False) or env['res.users'].browse(env.uid).lang
    lang = env['res.lang'].search([('code', '=', l)], limit=1)
    if lang:
        return date.strftime(lang.date_format)
    return date.strftime(DATE_FORMAT)


@staticmethod
def datetime_from_string_to_lang(value, env):
    if isinstance(value, basestring):
        date = fields.Datetime.from_string(value)
    else:
        date = value
    l = env.context.get('lang', False) or env['res.users'].browse(env.uid).lang
    lang = env['res.lang'].search([('code', '=', l)], limit=1)
    if lang:
        return date.strftime("%s %s" % (lang.date_format, lang.time_format))
    return date.strftime("%s %s" % (DATE_FORMAT, TIME_FORMAT))


@staticmethod
def datetime_to_user_tz(value, env):
    if isinstance(value, basestring):
        datetime_value = fields.Datetime.from_string(value)
    else:
        datetime_value = value
    if not isinstance(datetime_value, datetime.datetime):
        return value

    datetime_value = datetime_value.replace(tzinfo=pytz.utc)

    zone = env.context.get('tz', False) or env['res.users'].browse(
            env.uid).partner_id.tz or False
    if zone:
        tz = pytz.timezone(zone) or pytz.utc
    else:
        tz = pytz.utc
    return datetime_value.astimezone(tz)


@staticmethod
def datetime_time_as_float(value):
    if isinstance(value, basestring):
        for_date = fields.Datetime.from_string(value)
    else:
        for_date = value

    if not isinstance(for_date, datetime.datetime):
        return value

    date_only = datetime.datetime.combine(for_date.date(),
                                          datetime.time.min)
    date_only = date_only.replace(tzinfo=for_date.tzinfo)
    return round((for_date - date_only).total_seconds() / 3600, 3)


@staticmethod
def float_as_datetime(value, date=datetime.date.today()):
    seconds = value * 3600
    minutes, sec = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    time_value = datetime.time(int(hours), int(minutes), int(sec))
    if isinstance(date, basestring):
        date = fields.Date.from_string(date)
    datetime_value = datetime.datetime.combine(date, time_value)
    return datetime_value


@staticmethod
def float_to_time_lang(value, env):
    seconds = value * 3600
    minutes, sec = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    time_value = datetime.time(int(hours), int(minutes), int(sec))
    l = env.context.get('lang', False) or env['res.users'].browse(env.uid).lang
    lang = env['res.lang'].search([('code', '=', l)], limit=1)
    if lang:
        return time_value.strftime(lang.time_format)
    return time_value.strftime(TIME_FORMAT)


@staticmethod
def next_isoweekday(base_date, isoweekday):
    if isinstance(base_date, basestring):
        base_date = fields.Date.from_string(base_date)
    days_ahead = isoweekday - base_date.isoweekday()
    if days_ahead <= 0:  # Target day already happened this week
        days_ahead += 7
    return base_date + datetime.timedelta(days_ahead)


fields.Date.from_string_to_lang = date_from_string_to_lang
fields.Date.next_isoweekday = next_isoweekday
fields.Datetime.from_string_to_lang = datetime_from_string_to_lang
fields.Datetime.to_user_tz = datetime_to_user_tz
fields.Datetime.time_as_float = datetime_time_as_float
fields.Float.to_time_lang = float_to_time_lang
fields.Float.as_datetime = float_as_datetime
