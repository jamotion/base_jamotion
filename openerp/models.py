# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2004-2010 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo on 19.07.18.
#

import logging

from openerp import api
from openerp.models import BaseModel

_logger = logging.getLogger(__name__)

original_name_search = BaseModel.name_search


@api.model
def name_search(self, name='', args=None, operator='ilike', limit=100):
    """
    Hack of method for having a dynamic domain filter which is working
    in create and edit mode.
    
    Limitations:
    ------------
    Tested with m2o and m2m fields but not yet with o2m fields
    
    Usage:
    ------
    Define context at the view with following pattern:
    
    context="{'filter_by_<table_name>.<field_name>': <value>}"/>
    
      -> filter_by_ :: prefix for dynamic domain filter
      -> <table_name> :: comodel name (with underscore instead of .)
      -> <field_name> :: field name of comodel for search
      -> <value> :: the value to search in comodel field
     
    Example:
    --------
    <field name="rent_object_id"
           context="{'filter_by_rent_object.category_id': rent_category_id}"/>

        -> field rent_object_id is many2one with comodel rent_object
        -> category_id is the field name used for filter
        -> the value of field rent_category_id is used to filter
    """
    for k, v in self.env.context.iteritems():
        if not v or k[:10] != 'filter_by_' or '.' not in k[10:]:
            continue

        model, field_name = k[10:].split('.')
        if self._table != model or not hasattr(self, field_name):
            continue

        field = self._fields[field_name]
        if field.type not in ['many2one', 'many2many']:
            continue

        if isinstance(v, list):
            v = v[0]
        if isinstance(v, (list, tuple)) and len(v) == 3:
            v = v[2]
        else:
            v = [int(v)]

        # filter_ids = self.env[field.comodel_name].browse(v).ids
        args.append((field_name, 'in', v))

    return original_name_search(self, name, args, operator, limit)


BaseModel.name_search = name_search
