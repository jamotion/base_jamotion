# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 20.03.2017.
#
import logging

import openerp
import openerp.tests

from openerp.tests import common
from openerp.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class TestAddressPrint(common.TransactionCase):
    def setUp(self):
        super(TestAddressPrint, self).setUp()
        self.test_country = self.env['res.country'].create({
            'name': 'Fantasia',
        })
        self.test_partner = self.env['res.partner'].create({
            'name': 'Peter Pan',
            'zip': '12345',
            'city': 'London',
            'country_id': self.test_country.id,
        })

    def test_line_generation_valid_entries(self):
        test_entries = [
            ('(",", [adr.name])', 'Peter Pan'),
            ('(",", [adr.name, adr.city])', 'Peter Pan,London'),
            ('(", ", [adr.name, adr.city])', 'Peter Pan, London'),
            ('(" ", [adr.zip, adr.city])', '12345 London'),
            ('[adr.zip, adr.city]', '12345 London'),
            ('adr.zip', '12345'),
            ('', False),
            (False, False),
        ]

        for test_entry in test_entries:
            result = self.test_country._generate_address_line(
                self.test_partner,
                test_entry[0],
            )
            self.assertEqual(test_entry[1], result)

    def test_line_generation_invalid_entries(self):

        test_entries = [
            ('(",", [adr.name]', 'Peter Pan'),
            ('(",", adr.name,adr.city])', 'Peter Pan,London'),
            ('[adr.name, adr.city])', 'Peter Pan, London'),
        ]

        for test_entry in test_entries:
            with self.assertRaises(SyntaxError):
                result = self.test_country._generate_address_line(
                        self.test_partner,
                        test_entry[0]
                )

    def test_block_generation_valid_entries(self):
            test_entries = [
                ('adr.name\n[adr.zip, adr.city]\nadr.country_id.name',
                 'Peter Pan\n12345 London\nFantasia'),
                ('adr.city', 'London'),
                ('', False),
                (False, False),
            ]

            for test_entry in test_entries:
                self.test_country.address_block_base = test_entry[0]
                result = self.test_country.generate_address_block(
                    self.test_partner,
                )
                self.assertEqual(test_entry[1], result)

    def test_compute_address(self):
            result = self.test_partner._compute_address_for_print()
